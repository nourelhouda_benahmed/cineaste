﻿using Cineaste.Data.Models;
using Cineaste.Service.moviesService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;

namespace Cineaste.Web.Controllers
{
    [RequireHttps]
    public class MainPageController : Controller
    {
        IMovieService movieService=null;

        public MainPageController()
        {
            movieService=new MovieService();
        }
        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        movieService.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
        //
        // GET: /MainPage/
        public ActionResult Index()
        {
            

            var allMovies = movieService.GetMovies();
            return View(allMovies.ToList());
        }

        public ActionResult MoviesIndex()
        {
            return View();
        }

        //
        // GET: /MainPage/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /MainPage/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /MainPage/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /MainPage/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /MainPage/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /MainPage/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /MainPage/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
