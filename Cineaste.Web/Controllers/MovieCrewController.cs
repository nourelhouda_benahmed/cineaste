﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cineaste.Data.Models;

namespace Cineaste.Web.Controllers
{
    public class MovieCrewController : Controller
    {
        private cineastdbContext db = new cineastdbContext();

        // GET: /MovieCrew/
        public ActionResult Index()
        {
            return View(db.moviecrews.ToList());
        }

        // GET: /MovieCrew/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            moviecrew moviecrew = db.moviecrews.Find(id);
            if (moviecrew == null)
            {
                return HttpNotFound();
            }
            return View(moviecrew);
        }

        // GET: /MovieCrew/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /MovieCrew/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id,name")] moviecrew moviecrew)
        {
            if (ModelState.IsValid)
            {
                db.moviecrews.Add(moviecrew);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(moviecrew);
        }

        // GET: /MovieCrew/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            moviecrew moviecrew = db.moviecrews.Find(id);
            if (moviecrew == null)
            {
                return HttpNotFound();
            }
            return View(moviecrew);
        }

        // POST: /MovieCrew/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,name")] moviecrew moviecrew)
        {
            if (ModelState.IsValid)
            {
                db.Entry(moviecrew).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(moviecrew);
        }

        // GET: /MovieCrew/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            moviecrew moviecrew = db.moviecrews.Find(id);
            if (moviecrew == null)
            {
                return HttpNotFound();
            }
            return View(moviecrew);
        }

        // POST: /MovieCrew/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            moviecrew moviecrew = db.moviecrews.Find(id);
            db.moviecrews.Remove(moviecrew);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
