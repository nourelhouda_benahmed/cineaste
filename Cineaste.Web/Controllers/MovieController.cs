﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Cineaste.Data.Models;

namespace Cineaste.Web.Controllers
{
    public class MovieController : Controller
    {
        private cineastdbContext db = new cineastdbContext();

        // GET: /Movie/
        public ActionResult Index()
        {
            var movies = db.movies.Include(m => m.boxoffice).Include(m => m.user);
            return View(movies.ToList());
        }

        // GET: /Movie/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            movie movie = db.movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        // GET: /Movie/Create
        public ActionResult Create()
        {
            ViewBag.boxoffice_id = new SelectList(db.boxoffices, "id", "type");
            ViewBag.owner_id = new SelectList(db.users, "id", "address");
            return View();
        }

        // POST: /Movie/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="id,budget,dateOfRealise,genre,nb,sommeNb,photo,signalisation,synopsis,theme,title,trailer,boxoffice_id,owner_id")] movie movie)
        {
            if (ModelState.IsValid)
            {
                db.movies.Add(movie);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.boxoffice_id = new SelectList(db.boxoffices, "id", "type", movie.boxoffice_id);
            ViewBag.owner_id = new SelectList(db.users, "id", "address", movie.owner_id);
            return View(movie);
        }

        // GET: /Movie/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            movie movie = db.movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            ViewBag.boxoffice_id = new SelectList(db.boxoffices, "id", "type", movie.boxoffice_id);
            ViewBag.owner_id = new SelectList(db.users, "id", "address", movie.owner_id);
            return View(movie);
        }

        // POST: /Movie/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="id,budget,dateOfRealise,genre,nb,SommeNb,photo,signalisation,synopsis,theme,title,trailer,boxoffice_id,owner_id")] movie movie)
        {
            if (ModelState.IsValid)
            {
                db.Entry(movie).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.boxoffice_id = new SelectList(db.boxoffices, "id", "type", movie.boxoffice_id);
            ViewBag.owner_id = new SelectList(db.users, "id", "address", movie.owner_id);
            return View(movie);
        }

        // GET: /Movie/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            movie movie = db.movies.Find(id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        // POST: /Movie/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            movie movie = db.movies.Find(id);
            db.movies.Remove(movie);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
