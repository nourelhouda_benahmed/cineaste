﻿using Cineaste.Data.Models;
using Cineaste.Service.usersServices;
using Cineaste.Web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace uFundingPIDEV.Web.Controllers
{
    public class AuthenticationController : Controller
    {
        private UserService cs;
     
        IAuthenticationManager Authentication
        {
            get { return HttpContext.GetOwinContext().Authentication; }

        }
        public AuthenticationController()
        {
            cs = new UserService();
           
        }

        [HttpGet]
        public ActionResult Show()
        {
            return View();
        }

        [Authorize]
        public ActionResult Manage()
        {
            user u = cs.getuByUserName(User.Identity.Name);
            ViewBag.status = "";
            return View(u);
        }
        [Authorize]
        [HttpPost]
        public ActionResult Manage(user u)
        {

            if (u != null)
            {
                user res = cs.getuByUserName(u.email);
                
                res.firstName = u.firstName;
                res.lastName = u.lastName;

                cs.UpdateUser(res);
                ViewBag.status = "update success";
                return View(res);
            }

            return View(u);

        }
       
        public ActionResult login(String returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        [HttpPost]

        public ActionResult Login(user input, String returnUrl)
        {
            if (input.email != null)
            {
                input.password = CalculateMD5Hash(input.password);
                var v = cs.getuByUserNamePassword(input.email, input.password);
                if (v != null)
                {
                    var identity = new ClaimsIdentity(new[] {
                            new Claim(ClaimTypes.Name, input.email),
                        },
                        DefaultAuthenticationTypes.ApplicationCookie,
                        ClaimTypes.Name, ClaimTypes.Role);

                    // if you want roles, just add as many as you want here (for loop maybe?)
                    identity.AddClaim(new Claim(ClaimTypes.Role, "cineast"));
                    // tell OWIN the identity provider, optional
                    // identity.AddClaim(new Claim(IdentityProvider, "Simplest Auth"));

                    Authentication.SignIn(new AuthenticationProperties
                    {
                        
                    }, identity);
                    return RedirectToLocal(returnUrl);

                }
            }
            ModelState.AddModelError("", "Invalid username or password.");

            return View(input);
        }
 

        
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult Register(user model)
        {
            if (model != null)
            {
                var u = cs.getuByUserName(model.email);
                if (u == null)
                {
                    user c = new user();
                    c.email = model.email;
                    
                    c.password = CalculateMD5Hash(model.password);
                    cs.CreateUser(c);
                    //   await SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Home");
                }
                ViewBag.Error = "This email already exists";

            }
            return View(model);
        }

        public string CalculateMD5Hash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }


       
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Authentication", new { ReturnUrl = returnUrl }));
        }
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await Authentication.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }


            // Sign in the user with this external login provider if the user already has a login

            var user1 = cs.getuByUserName(loginInfo.Login.ToString());
            if (user1 != null)
            {
                // await SignInAsync(user1r, isPersistent: false);
                var identity = new ClaimsIdentity(new[] {
                            new Claim(ClaimTypes.Name,user1.email),
                        },
               DefaultAuthenticationTypes.ApplicationCookie,
               ClaimTypes.Name, ClaimTypes.Role);

                // if you want roles, just add as many as you want here (for loop maybe?)
                identity.AddClaim(new Claim(ClaimTypes.Role, "Cineast"));
                // tell OWIN the identity provider, optional
                // identity.AddClaim(new Claim(IdentityProvider, "Simplest Auth"));

                Authentication.SignIn(new AuthenticationProperties
                {
                    //  IsPersistent = input.RememberMe
                }, identity);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // If the user does not have an account, then prompt the user to create an account
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                // new ExternalLoginConfirmationViewModel { UserName = loginInfo.DefaultUserName }
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Login.ToString() });
            }
        }
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }
        public UserManager<ApplicationUser> UserManager { get; private set; }
        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            Authentication.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            Authentication.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);
        }
        [HttpPost]
        [AllowAnonymous]

     
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Manage");
            }

            if (model != null)
            {
                // Get the information about the user from the external login provider
                var info = await Authentication.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                //var user = new ApplicationUser() { UserName = model.UserName};
                //var result = await UserManager.CreateAsync(user);
                cs.CreateUser(new user { email = model.Email });
                user result = cs.getuByUserName(model.Email);
                if (result != null)
                {
                    //result = await UserManager.AddLoginAsync(user.Id+"", info.Login);
                    //if (result.Succeeded)
                    //{
                    var identity = new ClaimsIdentity(new[] {
                            new Claim(ClaimTypes.Name, result.email),
                        },
               DefaultAuthenticationTypes.ApplicationCookie,
               ClaimTypes.Name, ClaimTypes.Role);
                    identity.AddClaim(new Claim(ClaimTypes.Role, "Cineast"));
                    //await SignInAsync(user, isPersistent: false);
                    Authentication.SignIn(new AuthenticationProperties(), identity);
                    return RedirectToLocal(returnUrl);
                }
            }
            // AddErrors(result);
            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        public ActionResult Logout()
        {
            user c = cs.getuByUserName(User.Identity.Name);
            //c.isconnected = false;
            //cs.modifyu(c);
            Authentication.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("index", "home");
        }
        #region challenge
        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        private const string XsrfKey = "XsrfId";
        #endregion



       
    }
}