﻿using Cineaste.Data.Models;
using Cineaste.Service.moviesService;
using Cineaste.Service.moviesServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.IO;
using Cineaste.Domain;

namespace Cineaste.Web.Controllers
{
    public class MoviesController : Controller
    {

        IMovieService movieService = null;
       
        public MoviesController()
        {
            movieService = new MovieService();
        }
        //
        // GET: /Movies/
        public ActionResult Index(string sortOrder, string currentFilter, string searchString, int? page)
        {
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            var allMovies = movieService.GetMovies();

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewBag.CurrentFilter = searchString;


            if (!String.IsNullOrEmpty(searchString))
            {
                allMovies = allMovies.Where(s => s.title.Contains(searchString)
                                       || s.genre.Contains(searchString));
            }


            return View(allMovies.ToPagedList(pageNumber, pageSize));
        }

        //
        // GET: /Movies/Details/5
        public ActionResult Details(int id)
        {

            var movie = movieService.GetMovie(id);
            return View(movie);
        }

        //
        // GET: /Movies/Create

        public ActionResult Create()
        {

            return View();
        }

     
        //upload*********************************************
        [HttpPost]
        [ValidateAntiForgeryToken]
        public void  Upload(HttpPostedFileBase photo)
        {
            string directory = @"E:\esprit\4GL1\PI-Dev\server\PiDevServer\jboss-as-7.1.1.Final\welcome-content\image\";

            if (photo != null && photo.ContentLength > 0)
            {
                string fileName = Path.GetFileName(photo.FileName);
                transaction.getInstanceOf().ImageName = fileName;
                photo.SaveAs(Path.Combine(directory, fileName));
            }

            
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public void Uploadvideo(HttpPostedFileBase video)
        {
            string directory2 = @"E:\esprit\4GL1\PI-Dev\server\PiDevServer\jboss-as-7.1.1.Final\welcome-content\Video\";

            if (video != null && video.ContentLength > 0)
            {
                string fileName2 = Path.GetFileName(video.FileName);
                transaction.getInstanceOf().VideoName = fileName2;
                video.SaveAs(Path.Combine(directory2, fileName2));
            }

            
        }

       
        //********************************
        //
        // POST: /Movies/Create
        [HttpPost]
      
        public ActionResult Create(movie newMovie)
        {

            // TODO: Add insert logic here
            //formulaire
            
            if (ModelState.IsValid)
            {
                newMovie.ImgUrl = "image/" + transaction.getInstanceOf().ImageName;
                newMovie.trailer = transaction.getInstanceOf().VideoName.Remove(transaction.getInstanceOf().VideoName.Length-4);

            movieService.CreateMovie(newMovie);
            movieService.Commit();
            return RedirectToAction("Index");

            }
            return View();

        }

        //
        // GET: /Movies/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /Movies/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /Movies/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /Movies/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
