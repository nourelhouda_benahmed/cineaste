﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using Microsoft.Owin.Security;
namespace Cineaste.Web
{
    public partial class Startup
    {
        // For more information on configuring authentication, please visit http://go.microsoft.com/fwlink/?LinkId=301864
        public void ConfigureAuth(IAppBuilder app)
        {

            // Enable the application to use a cookie to store information for the signed in user
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Authentication/Login")
            });
            // Use a cookie to temporarily store information about a user logging in with a third party login provider

            var cookieOptions = new CookieAuthenticationOptions
            {
                LoginPath = new PathString("/Authentication/ExternalLogin")
            };
            //app.SetDefaultSignInAsAuthenticationType(cookieOptions.AuthenticationType);
            app.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

            //app.SetDefaultSignInAsAuthenticationType(DefaultAuthenticationTypes.ExternalCookie);
            // Uncomment the following lines to enable logging in with third party login providers
            //app.UseMicrosoftAccountAuthentication(
            // clientId: "",
            // clientSecret: "");

            //app.UseTwitterAuthentication(
            // consumerKey: "",
            // consumerSecret: "");

            //app.UseFacebookAuthentication(
            // appId: "",
            // appSecret: "");

            //app.UseGoogleAuthentication();
            //app.UseFacebookAuthentication(
            //appId: "351758685005990",
            //appSecret: "df0d8f21403d1b54e3227dcd4c9b3a7c");

            app.UseGoogleAuthentication(
            clientId: "808869805722-vi735lr7eqg793apio7nb7u4lrulih0j.apps.googleusercontent.com",
            clientSecret: "0XnbuqSFI-3nXcCk7Ey1tQ2R");
        }

    }
}