﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Cineaste.Web.Startup))]
namespace Cineaste.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
