﻿using Cineaste.Data.Models;


using Cineaste.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyFinance.Data.Infrastructure;
using MyFInance.Data.Infrastructure;

namespace Cineaste.Service.moviecrewService

{
    public class moviecrewService : IMoviecrewService
    {
        DatabaseFactory dbFactory = null;
        IUnitOfWork utOfWork = null;
        public moviecrewService()
        {
            dbFactory = new DatabaseFactory();
            utOfWork = new UnitOfWork(dbFactory);
        }
        public void CreateMoviecrew(moviecrew m)
        {
            throw new NotImplementedException();
        }

        public moviecrew GetMoviecrew(int id)
        {
            throw new NotImplementedException();
        }

        public  IEnumerable<moviecrew> GetMoviecrew()
        {
            var moviecrews = utOfWork.MoviecrewRepository.GetAll();
            return moviecrews;
        }

        public IEnumerable<moviecrew> GetMoviecrewbyMember(int MemberId)
        {
            throw new NotImplementedException();
        }

        public void Updatemoviecrew(moviecrew mc)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            utOfWork.Dispose();
        }
    }
}
