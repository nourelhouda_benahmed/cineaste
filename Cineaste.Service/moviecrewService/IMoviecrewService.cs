﻿using Cineaste.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Cineaste.Service.moviecrewService

{
    public interface IMoviecrewService : IDisposable
    {
        void CreateMoviecrew(moviecrew mc);
        moviecrew GetMoviecrew(int id);
        IEnumerable<moviecrew> GetMoviecrew();
        IEnumerable<moviecrew> GetMoviecrewbyMember(int MemberId);
        void Updatemoviecrew(moviecrew mc);
    }
}
