﻿using Cineaste.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cineaste.Service.usersServices
{
    public interface IUserService : IDisposable
    {
        void CreateUser(user u);
        user GetUser(int id);
        IEnumerable<user> GetAllUsers();
        IEnumerable<user> GetUsersByRole();
        void UpdateUser(user u);
        void DeleteUser(int id);
    }
}
