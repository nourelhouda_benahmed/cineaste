﻿using Cineaste.Data.Models;
using MyFinance.Data.Infrastructure;
using MyFInance.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cineaste.Service.usersServices
{
    public class UserService : IUserService
    {
        IDatabaseFactory dbFactory = null;
        UnitOfWork utOfWork = null;

        public UserService()
        {
            dbFactory = new DatabaseFactory();
            utOfWork = new UnitOfWork(dbFactory);
        }
        public void CreateUser(user u)
        {
            utOfWork.UserRepository.Add(u);
            utOfWork.Commit();
        }

        public user GetUser(int id)
        {
            return utOfWork.UserRepository.GetById(id);
        }

        public IEnumerable<user> GetAllUsers()
        {
            return utOfWork.UserRepository.GetAll();
        }

        public IEnumerable<user> GetUsersByRole()
        {
            throw new NotImplementedException();
        }

        public void UpdateUser(user u)
        {
            utOfWork.UserRepository.Update(u);
            utOfWork.Commit();
        }

        public void DeleteUser(int id)
        {
            utOfWork.UserRepository.Delete(id);
            utOfWork.Commit();
        }
        public user getuByUserName(string userName)
        {
            return utOfWork.UserRepository.Get(p => p.email.Equals(userName));
        }
        public user getuByUserNamePassword(string userName, string password)
        {
            return utOfWork.UserRepository.Get(p => p.email.Equals(userName)&& p.password.Equals(password));
        }
        public void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
