﻿using Cineaste.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace Cineaste.Service.moviesService
{

    public interface IMovieService :IDisposable
    {
        void CreateMovie(movie m);
        movie GetMovie(int id);
        IEnumerable<movie> GetMovies();
        IEnumerable<movie> GetMovieByUser(int userId);
        void UpdateMovie(movie m);
        void Commit();
    }
}
