﻿using Cineaste.Data.Models;
using Cineaste.Service.moviesService;

using Cineaste.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MyFinance.Data.Infrastructure;
using MyFInance.Data.Infrastructure;


namespace Cineaste.Service.moviesService
{
    public class MovieService :IMovieService
    {
        DatabaseFactory dbFactory = null;
        IUnitOfWork utOfWork = null;
        public MovieService()
        {
            dbFactory = new DatabaseFactory();
            utOfWork = new UnitOfWork(dbFactory);
        }
        public void CreateMovie(movie m)
        {
            utOfWork.MovieRepository.Add(m);
            
        }

        public movie GetMovie(int id)
        {
            var m = utOfWork.MovieRepository.GetById(id);
            return m;
        }

        public IEnumerable<movie> GetMovies() 
        {
            var movies = utOfWork.MovieRepository.GetAll();
            return movies;
        }

        public IEnumerable<movie> GetMovieByUser(int userId)
        {
            throw new NotImplementedException();
        }

        public void UpdateMovie(movie m)
        {
            throw new NotImplementedException();
        }

        public void Dispose()
        {
            utOfWork.Dispose();
        }

        public void Commit()
        {
            utOfWork.Commit();
        }
    }//class
}//namespace
