﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cineaste.Domain
{
    public class transaction
    {
        private string imageName;

        public string ImageName
        {
            get { return imageName; }
            set { imageName = value; }
        }
        private string videoName;

        public string VideoName
        {
            get { return videoName; }
            set { videoName = value; }
        }

        private static transaction instanceOf;

        private transaction()
        {

        }
        public static transaction getInstanceOf()
        {
            if (instanceOf == null)
            {
                instanceOf = new transaction();
            }
            return instanceOf;
        }

    }
}
