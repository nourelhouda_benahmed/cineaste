using System;
using System.Collections.Generic;

namespace Cineaste.Data.Models
{
    public partial class boxoffice
    {
        public boxoffice()
        {
            this.movies = new List<movie>();
        }

        public int id { get; set; }
        public string type { get; set; }
        public virtual ICollection<movie> movies { get; set; }
    }
}
