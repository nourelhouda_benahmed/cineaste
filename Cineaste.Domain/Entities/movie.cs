using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cineaste.Data.Models
{
    public partial class movie
    {
        public int id { get; set; }
        public double budget { get; set; }
        public Nullable<System.DateTime> dateOfRealise { get; set; }
        public string genre { get; set; }
        public int nb { get; set; }
        public int sommeNb { get; set; }
        public byte[] photo { get; set; }
        public int signalisation { get; set; }
        public string synopsis { get; set; }
        public string theme { get; set; }
        [Required]
        public string title { get; set; }
        public string trailer { get; set; }
        public string ImgUrl { get; set; }
        public Nullable<int> boxoffice_id { get; set; }
        public Nullable<int> owner_id { get; set; }
        
        public virtual boxoffice boxoffice { get; set; }
        
        public virtual user user { get; set; }
    }
}
