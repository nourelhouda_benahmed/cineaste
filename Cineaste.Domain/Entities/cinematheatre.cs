using System;
using System.Collections.Generic;

namespace Cineaste.Data.Models
{
    public partial class cinematheatre
    {
        public int id { get; set; }
        public string adress { get; set; }
        public int placeNumber { get; set; }
    }
}
