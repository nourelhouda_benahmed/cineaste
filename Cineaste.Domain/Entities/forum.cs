using System;
using System.Collections.Generic;

namespace Cineaste.Data.Models
{
    public partial class forum
    {
        public forum()
        {
            this.subjects = new List<subject>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public virtual ICollection<subject> subjects { get; set; }
    }
}
