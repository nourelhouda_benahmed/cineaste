using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Cineaste.Data.Models
{
    public partial class subject
    {
        public subject()
        {
            this.messages = new List<message>();
        }

        [Key]
        public int idSubject { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public string description { get; set; }
        public string time { get; set; }
        public string title { get; set; }
        public Nullable<int> forum_id { get; set; }
        public virtual forum forum { get; set; }
        public virtual ICollection<message> messages { get; set; }
    }
}
