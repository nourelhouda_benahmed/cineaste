using System;
using System.Collections.Generic;

namespace Cineaste.Data.Models
{
    public partial class membre
    {
        public int id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string role { get; set; }
    }
}
