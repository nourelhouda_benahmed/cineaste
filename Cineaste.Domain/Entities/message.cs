using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Cineaste.Data.Models
{
    public partial class message
    {
        [Key, Column(Order = 0)]
        public int subjectId { get; set; }
        [Key, Column(Order = 1)]
        public int userId { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public int likes { get; set; }
        public int signalisation { get; set; }
        public string text { get; set; }
        public virtual user user { get; set; }
        public virtual subject subject { get; set; }
    }
}
