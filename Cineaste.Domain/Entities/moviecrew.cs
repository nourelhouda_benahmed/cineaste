using System;
using System.Collections.Generic;

namespace Cineaste.Data.Models
{
    public partial class moviecrew
    {
        public int id { get; set; }
        public string name { get; set; }
    }
}
