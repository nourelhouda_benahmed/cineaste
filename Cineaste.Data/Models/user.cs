using System;
using System.Collections.Generic;

namespace Cineaste.Data.Models
{
    public partial class user
    {
        public user()
        {
            this.messages = new List<message>();
            this.movies = new List<movie>();
        }

        public int id { get; set; }
        public string address { get; set; }
        public string country { get; set; }
        public Nullable<System.DateTime> dateBirth { get; set; }
        public Nullable<System.DateTime> dateOfInscription { get; set; }
        public string email { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string password { get; set; }
        public string phone { get; set; }
        public byte[] picture { get; set; }
        public virtual ICollection<message> messages { get; set; }
        public virtual ICollection<movie> movies { get; set; }
    }
}
