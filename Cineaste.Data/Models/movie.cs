using System;
using System.Collections.Generic;

namespace Cineaste.Data.Models
{
    public partial class movie
    {
        public movie()
        {
            this.cinematheatres = new List<cinematheatre>();
        }

        public int id { get; set; }
        public double budget { get; set; }
        public Nullable<System.DateTime> dateOfRealise { get; set; }
        public string genre { get; set; }
        public int nb { get; set; }
        public byte[] photo { get; set; }
        public int signalisation { get; set; }
        public string synopsis { get; set; }
        public string theme { get; set; }
        public string title { get; set; }
        public string trailer { get; set; }
        public Nullable<int> owner_id { get; set; }
        public virtual user user { get; set; }
        public virtual ICollection<cinematheatre> cinematheatres { get; set; }
    }
}
