using System;
using System.Collections.Generic;

namespace Cineaste.Data.Models
{
    public partial class message
    {
        public int subjectId { get; set; }
        public int userId { get; set; }
        public Nullable<System.DateTime> date { get; set; }
        public int likes { get; set; }
        public int signalisation { get; set; }
        public string text { get; set; }
        public virtual user user { get; set; }
        public virtual subject subject { get; set; }
    }
}
