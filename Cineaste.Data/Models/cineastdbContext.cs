using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Cineaste.Data.Models.Mapping;

namespace Cineaste.Data.Models
{
    public partial class cineastdbContext : DbContext
    {
        static cineastdbContext()
        {
            Database.SetInitializer<cineastdbContext>(null);
        }

        public cineastdbContext()
            : base("name = DefaultConnection")
        {
           
        }

        public DbSet<boxoffice> boxoffices { get; set; }
        public DbSet<cinematheatre> cinematheatres { get; set; }
        public DbSet<forum> forums { get; set; }
        public DbSet<membre> membres { get; set; }
        public DbSet<message> messages { get; set; }
        public DbSet<moviecrew> moviecrews { get; set; }
        public DbSet<movie> movies { get; set; }
        
        public DbSet<subject> subjects { get; set; }
        public DbSet<user> users { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new boxofficeMap());
            modelBuilder.Configurations.Add(new cinematheatreMap());
            modelBuilder.Configurations.Add(new forumMap());
            modelBuilder.Configurations.Add(new membreMap());
            modelBuilder.Configurations.Add(new messageMap());
            modelBuilder.Configurations.Add(new moviecrewMap());
            modelBuilder.Configurations.Add(new movieMap());
            
            modelBuilder.Configurations.Add(new subjectMap());
            modelBuilder.Configurations.Add(new userMap());
        }
    }
}
