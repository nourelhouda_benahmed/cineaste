using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cineaste.Data.Models.Mapping
{
    public class movieMap : EntityTypeConfiguration<movie>
    {
        public movieMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.genre)
                .HasMaxLength(255);

            this.Property(t => t.synopsis)
                .HasMaxLength(255);

            this.Property(t => t.theme)
                .HasMaxLength(255);

            this.Property(t => t.title)
                .HasMaxLength(255);

            this.Property(t => t.trailer)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("movies", "cineastdb");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.budget).HasColumnName("budget");
            this.Property(t => t.dateOfRealise).HasColumnName("dateOfRealise");
            this.Property(t => t.genre).HasColumnName("genre");
            this.Property(t => t.nb).HasColumnName("nb");
            this.Property(t => t.sommeNb).HasColumnName("sommeNb");
            this.Property(t => t.ImgUrl).HasColumnName("imgUrl");
            this.Property(t => t.photo).HasColumnName("photo");
            this.Property(t => t.signalisation).HasColumnName("signalisation");
            this.Property(t => t.synopsis).HasColumnName("synopsis");
            this.Property(t => t.theme).HasColumnName("theme");
            this.Property(t => t.title).HasColumnName("title");
            this.Property(t => t.trailer).HasColumnName("trailer");
            this.Property(t => t.boxoffice_id).HasColumnName("boxoffice_id");
            this.Property(t => t.owner_id).HasColumnName("owner_id");
          

            // Relationships
            this.HasOptional(t => t.boxoffice)
                .WithMany(t => t.movies)
                .HasForeignKey(d => d.boxoffice_id);
           
            this.HasOptional(t => t.user)
                .WithMany(t => t.movies)
                .HasForeignKey(d => d.owner_id);

        }
    }
}
