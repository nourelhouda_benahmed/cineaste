using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cineaste.Data.Models.Mapping
{
    public class subjectMap : EntityTypeConfiguration<subject>
    {
        public subjectMap()
        {
            // Primary Key
            this.HasKey(t => t.idSubject);

            // Properties
            this.Property(t => t.description)
                .HasMaxLength(255);

            this.Property(t => t.time)
                .HasMaxLength(255);

            this.Property(t => t.title)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("subject", "cineastdb");
            this.Property(t => t.idSubject).HasColumnName("idSubject");
            this.Property(t => t.date).HasColumnName("date");
            this.Property(t => t.description).HasColumnName("description");
            this.Property(t => t.time).HasColumnName("time");
            this.Property(t => t.title).HasColumnName("title");
            this.Property(t => t.forum_id).HasColumnName("forum_id");

            // Relationships
            this.HasOptional(t => t.forum)
                .WithMany(t => t.subjects)
                .HasForeignKey(d => d.forum_id);

        }
    }
}
