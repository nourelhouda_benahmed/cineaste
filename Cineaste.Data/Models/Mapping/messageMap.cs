using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cineaste.Data.Models.Mapping
{
    public class messageMap : EntityTypeConfiguration<message>
    {
        public messageMap()
        {
            // Primary Key
            this.HasKey(t => new { t.subjectId, t.userId });

            // Properties
            this.Property(t => t.subjectId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.userId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.text)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("message", "cineastdb");
            this.Property(t => t.subjectId).HasColumnName("subjectId");
            this.Property(t => t.userId).HasColumnName("userId");
            this.Property(t => t.date).HasColumnName("date");
            this.Property(t => t.likes).HasColumnName("likes");
            this.Property(t => t.signalisation).HasColumnName("signalisation");
            this.Property(t => t.text).HasColumnName("text");

            // Relationships
            this.HasRequired(t => t.user)
                .WithMany(t => t.messages)
                .HasForeignKey(d => d.userId);
            this.HasRequired(t => t.subject)
                .WithMany(t => t.messages)
                .HasForeignKey(d => d.subjectId);

        }
    }
}
