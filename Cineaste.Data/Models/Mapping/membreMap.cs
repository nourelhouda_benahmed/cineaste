using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cineaste.Data.Models.Mapping
{
    public class membreMap : EntityTypeConfiguration<membre>
    {
        public membreMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.firstName)
                .HasMaxLength(255);

            this.Property(t => t.lastName)
                .HasMaxLength(255);

            this.Property(t => t.role)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("membres", "cineastdb");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.firstName).HasColumnName("firstName");
            this.Property(t => t.lastName).HasColumnName("lastName");
            this.Property(t => t.role).HasColumnName("role");
        }
    }
}
