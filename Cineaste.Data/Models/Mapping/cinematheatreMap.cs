using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Cineaste.Data.Models.Mapping
{
    public class cinematheatreMap : EntityTypeConfiguration<cinematheatre>
    {
        public cinematheatreMap()
        {
            // Primary Key
            this.HasKey(t => t.id);

            // Properties
            this.Property(t => t.id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.adress)
                .HasMaxLength(255);

            // Table & Column Mappings
            this.ToTable("cinematheatres", "cineastdb");
            this.Property(t => t.id).HasColumnName("id");
            this.Property(t => t.adress).HasColumnName("adress");
            this.Property(t => t.placeNumber).HasColumnName("placeNumber");
        }
    }
}
