using System;
using System.Collections.Generic;

namespace Cineaste.Data.Models
{
    public partial class moviecrew
    {
        public moviecrew()
        {
            this.membres = new List<membre>();
        }

        public int id { get; set; }
        public string name { get; set; }
        public virtual ICollection<membre> membres { get; set; }
    }
}
