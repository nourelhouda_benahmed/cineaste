using System;
using System.Collections.Generic;

namespace Cineaste.Data.Models
{
    public partial class cinematheatre
    {
        public cinematheatre()
        {
            this.movies = new List<movie>();
        }

        public int id { get; set; }
        public string adress { get; set; }
        public string nameCT { get; set; }
        public int placeNumber { get; set; }
        public virtual ICollection<movie> movies { get; set; }
    }
}
