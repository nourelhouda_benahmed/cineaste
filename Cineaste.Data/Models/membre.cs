using System;
using System.Collections.Generic;

namespace Cineaste.Data.Models
{
    public partial class membre
    {
        public membre()
        {
            this.moviecrews = new List<moviecrew>();
        }

        public int id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string role { get; set; }
        public virtual ICollection<moviecrew> moviecrews { get; set; }
    }
}
