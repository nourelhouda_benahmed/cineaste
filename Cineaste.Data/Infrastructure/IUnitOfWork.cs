﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFInance.Data.Infrastructure
{
    public interface IUnitOfWork : IDisposable
    {
        void Commit();
        IUserRepository UserRepository { get; }
        IMovieRepository MovieRepository { get; }
        IForumRepository ForumRepository { get; }
        IMemberRepository MemberRepository { get; }
         IMoviecrewRepository MoviecrewRepository { get; }
    }
}
