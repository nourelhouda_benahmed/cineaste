﻿using Cineaste.Data.Models;
using MyFinance.Data;
using MyFinance.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFInance.Data.Infrastructure
{
    public abstract class RepositoryBase<T> : IRepository<T> where T : class
    {
        private cineastdbContext dataContext;
        public cineastdbContext DataContext
        {
            get
            {
                return dataContext = dbFactory.DataContext;
            }
        }

        private IDbSet<T> dbset;
        IDatabaseFactory dbFactory;
        public RepositoryBase(IDatabaseFactory dbFactory)
        {
            this.dbFactory = dbFactory;
            dbset = DataContext.Set<T>();
        }

        public virtual void Add(T entity)
        {
            dbset.Add(entity);
        }

        public virtual void Update(T entity)
        {
            dbset.Attach(entity);
            dataContext.Entry(entity).State = EntityState.Modified;
        }

        public virtual void Delete(int id)
        {
            var e = dbset.Find(id);
            dbset.Remove(e);
        }

        public virtual void Delete(string id)
        {
            var e = dbset.Find(id);
            dbset.Remove(e);
        }

        public virtual void Delete(System.Linq.Expressions.Expression<Func<T, bool>> where)
        {
            IEnumerable<T> entities = dbset.Where(where);
            foreach (T e in entities)
            {
                dbset.Remove(e);
            }
        }

        public virtual T GetById(int id)
        {
            return dbset.Find(id);
        }

        public virtual T GetById(string id)
        {
            return dbset.Find(id);
        }

        public virtual T Get(System.Linq.Expressions.Expression<Func<T, bool>> where)
        {
            return dbset.Where(where).FirstOrDefault();          
        }

        public virtual IEnumerable<T> GetAll()
        {
            return dbset.ToList();
        }

        public virtual IEnumerable<T> GetMany(System.Linq.Expressions.Expression<Func<T, bool>> where)
        {
            return dbset.Where(where).AsEnumerable();
        }
    }
}
