﻿using Cineaste.Data.Models;
using MyFinance.Data;
using MyFinance.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFInance.Data.Infrastructure
{
    public class UnitOfWork : IUnitOfWork
    {
        IDatabaseFactory dbFactory;
        cineastdbContext dataContext;

        public cineastdbContext DataContext
        {
            get { return dataContext = dbFactory.DataContext; }
        }


        public UnitOfWork(IDatabaseFactory dbFactory)
        {
            this.dbFactory = dbFactory;
        }
       

        private IUserRepository userRepository;
        public IUserRepository UserRepository
        {
            get { return userRepository = new UserRepository(dbFactory); }
        }

        private IMovieRepository movieRepository;
        public IMovieRepository MovieRepository
        {
            get { return movieRepository = new MovieRepository(dbFactory); }
        }

        private IForumRepository forumRepository;
        public IForumRepository ForumRepository
        {
            get { return forumRepository = new ForumRepository(dbFactory); }
        }
        private IMemberRepository memberRepository;
        public IMemberRepository MemberRepository
        {
            get { return memberRepository = new MemberRepository(dbFactory); }
        }
        private IMoviecrewRepository moviecrewRepository;
        public IMoviecrewRepository MoviecrewRepository
        {
            get { return moviecrewRepository = new MoviecrewRepository(dbFactory); }
        }
         

        public void Dispose()
        {
            dbFactory.Dispose();
        }

        public void Commit()
        {
            DataContext.SaveChanges();
        }
    }//class
}//namespace
