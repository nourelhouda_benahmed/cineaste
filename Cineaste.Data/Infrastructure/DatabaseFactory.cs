﻿using Cineaste.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFinance.Data.Infrastructure
{
   public class DatabaseFactory : Disposable, IDatabaseFactory
    {
       private cineastdbContext dataContext;
       public cineastdbContext DataContext
        {
            get { return dataContext; }
        }

        public DatabaseFactory()
        {
            dataContext = new cineastdbContext();
        }

        protected override void DisposeCore()
        {
            if (DataContext!=null)
            {
                DataContext.Dispose();
            }
        }
}
}

