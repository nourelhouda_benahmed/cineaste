﻿using Cineaste.Data.Models;
using MyFinance.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFInance.Data.Infrastructure
{
    public class MovieRepository : RepositoryBase<movie>, IMovieRepository
    {
        public MovieRepository(IDatabaseFactory dbFactory)
            : base(dbFactory)
        {

        }
    }
    public interface IMovieRepository : IRepository<movie>
    {

    }
}
