﻿using Cineaste.Data.Models;
using MyFinance.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyFInance.Data.Infrastructure
{
    public class MoviecrewRepository : RepositoryBase<moviecrew>, IMoviecrewRepository
    {
        public MoviecrewRepository(IDatabaseFactory dbFactory)
            : base(dbFactory)
        {

        }
    }
    public interface IMoviecrewRepository : IRepository<moviecrew>
    {

    }
}
