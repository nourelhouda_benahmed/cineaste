﻿using Cineaste.Data.Models;
using MyFinance.Data.Infrastructure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MyFInance.Data.Infrastructure
{
    public class MemberRepository : RepositoryBase<membre>, IMemberRepository
    {
        public MemberRepository(IDatabaseFactory dbFactory)
            : base(dbFactory)
        {

        }
    }

    public interface IMemberRepository : IRepository<membre>
    {

    }
}
