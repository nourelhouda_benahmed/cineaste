namespace Cineaste.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class test : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "boxoffice",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        type = c.String(maxLength: 255, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "movies",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        budget = c.Double(nullable: false),
                        dateOfRealise = c.DateTime(precision: 0),
                        genre = c.String(maxLength: 255, storeType: "nvarchar"),
                        sommeNb = c.Int(nullable: false),
                        photo = c.Binary(),
                        signalisation = c.Int(nullable: false),
                        synopsis = c.String(maxLength: 255, storeType: "nvarchar"),
                        theme = c.String(maxLength: 255, storeType: "nvarchar"),
                        title = c.String(maxLength: 255, storeType: "nvarchar"),
                        trailer = c.String(maxLength: 255, storeType: "nvarchar"),
                        ImgUrl = c.String(unicode: false),
                        boxoffice_id = c.Int(),
                        owner_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("boxoffice", t => t.boxoffice_id)
                .ForeignKey("users", t => t.owner_id)
                .Index(t => t.boxoffice_id)
                .Index(t => t.owner_id);
            
            CreateTable(
                "users",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        address = c.String(maxLength: 255, storeType: "nvarchar"),
                        country = c.String(maxLength: 255, storeType: "nvarchar"),
                        dateBirth = c.DateTime(precision: 0),
                        dateOfInscription = c.DateTime(precision: 0),
                        email = c.String(maxLength: 255, storeType: "nvarchar"),
                        firstName = c.String(maxLength: 255, storeType: "nvarchar"),
                        lastName = c.String(maxLength: 255, storeType: "nvarchar"),
                        password = c.String(maxLength: 255, storeType: "nvarchar"),
                        phone = c.String(maxLength: 255, storeType: "nvarchar"),
                        picture = c.Binary(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "message",
                c => new
                    {
                        subjectId = c.Int(nullable: false),
                        userId = c.Int(nullable: false),
                        date = c.DateTime(precision: 0),
                        likes = c.Int(nullable: false),
                        signalisation = c.Int(nullable: false),
                        text = c.String(maxLength: 255, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => new { t.subjectId, t.userId })
                .ForeignKey("subject", t => t.subjectId, cascadeDelete: true)
                .ForeignKey("users", t => t.userId, cascadeDelete: true)
                .Index(t => t.subjectId)
                .Index(t => t.userId);
            
            CreateTable(
                "subject",
                c => new
                    {
                        idSubject = c.Int(nullable: false, identity: true),
                        date = c.DateTime(precision: 0),
                        description = c.String(maxLength: 255, storeType: "nvarchar"),
                        time = c.String(maxLength: 255, storeType: "nvarchar"),
                        title = c.String(maxLength: 255, storeType: "nvarchar"),
                        forum_id = c.Int(),
                    })
                .PrimaryKey(t => t.idSubject)
                .ForeignKey("forum", t => t.forum_id)
                .Index(t => t.forum_id);
            
            CreateTable(
                "forum",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        name = c.String(maxLength: 255, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "cinematheatres",
                c => new
                    {
                        id = c.Int(nullable: false),
                        adress = c.String(maxLength: 255, storeType: "nvarchar"),
                        placeNumber = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "membres",
                c => new
                    {
                        id = c.Int(nullable: false),
                        firstName = c.String(maxLength: 255, storeType: "nvarchar"),
                        lastName = c.String(maxLength: 255, storeType: "nvarchar"),
                        role = c.String(maxLength: 255, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "moviecrew",
                c => new
                    {
                        id = c.Int(nullable: false),
                        name = c.String(maxLength: 255, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("movies", "owner_id", "users");
            DropForeignKey("message", "userId", "users");
            DropForeignKey("message", "subjectId", "subject");
            DropForeignKey("subject", "forum_id", "forum");
            DropForeignKey("movies", "boxoffice_id", "boxoffice");
            DropIndex("subject", new[] { "forum_id" });
            DropIndex("message", new[] { "userId" });
            DropIndex("message", new[] { "subjectId" });
            DropIndex("movies", new[] { "owner_id" });
            DropIndex("movies", new[] { "boxoffice_id" });
            DropTable("moviecrew");
            DropTable("membres");
            DropTable("cinematheatres");
            DropTable("forum");
            DropTable("subject");
            DropTable("message");
            DropTable("users");
            DropTable("movies");
            DropTable("boxoffice");
        }
    }
}
